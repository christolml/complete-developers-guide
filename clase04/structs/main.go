package main

import "fmt"

type contactInfo struct {
	email string
	zipCode int
	
}


type person struct {
	firstName string
	lastName string
	contactInfo
	// al igual que en js puedo nomas dejar contactInfo y ya estoy
	// haciendo referencia a la estructura e contactInfo, "contact contactInfo" asi estaa antes
}


func main(){
	
	// chris := person{"chris","Velaz"}
	// fmt.Println(chris)
	
	chris := person{
		firstName: "Christopher",
		lastName: "Velazquez",
		contactInfo: contactInfo{
			email: "Chris@hotmail.com",
			zipCode: 12345,
		},
	}
	
	chris.updateName("Chris")
	chris.print()
	
}

func (pointerToPerson *person) updateName(newFirstName string)  {
	(*pointerToPerson).firstName = newFirstName
}


func (p person) print(){
	fmt.Printf("%+v", p )
}










