package main

import "fmt"

func main() {



	/*	 other way
		var colors map[string]string

		colors := make(map[string]string)

		colors["white"] = "#ffffff"

		delete(colors, "white")
		*/


	// one way to create a map
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#gm542f",
		"white": "#fffff",
	}




	//fmt.Println(colors)

	printMap(colors)

}


func printMap(c map[string]string) {
	for color, value := range c {
		fmt.Println("The color is ",color," and the value is ",value)
	}
}
