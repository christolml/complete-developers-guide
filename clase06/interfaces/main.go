package main

import "fmt"

type bot interface {
	getGreeting() string
}

// These structures are made to create some functions to work with it
type englishBot struct {}
type spanishBot struct{}


func main(){
	eb := englishBot{}
	sb := spanishBot{}

	printGreeting(eb)
	printGreeting(sb)

}

func printGreeting(b bot) {
	fmt.Println(b.getGreeting())
}

// Previously, this function had a variable like "func (eb englishBot)" but
// the variable eb was deleted because it will not be used
func (englishBot) getGreeting() string {
	// VERY custom logic for generating an english greeting
	return "Hi there"
}


func (spanishBot) getGreeting() string {
	return "Que onda prro"
}
